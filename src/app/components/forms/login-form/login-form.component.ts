import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  user = {
    username: '',
    password: ''
  };

  constructor(private authService: AuthService, private router: Router) { }

  @Input() formHeaderText:string;
  @Input() formLogin:string;
  @Input() messageLogin: string;

  @Output() loginAttempt: EventEmitter<any> = new EventEmitter();

  onLoginClick() {

    const success = this.authService.login(this.user);

    console.log(success);

    if (success) {
      // redirect to the dashboard
      this.router.navigateByUrl('/dashboard');
    } else {
      alert("Hey, I dont know you!");
    }

    // console.log(this.user);
    // this.loginAttempt.emit('Pleace insert valid information');
  }


}
