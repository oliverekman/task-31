import { Component, Input, Output,  EventEmitter } from '@angular/core';
import { Xtb } from '@angular/compiler';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent {

  user = {
    username: '',
    password: '',
    confirmPassoword: ''
  };

  constructor(private userService: AuthService) { }

  @Input() messageRegister: string;
  @Input() formRegister:string;

  @Output() registerAttempt: EventEmitter<any> = new EventEmitter;

  async onRegisterClick() {

    try {
      console.log(this.user);

      const result: any = await this.userService.register(this.user);
      console.log(result);

    } catch (e){
      console.error(e);

    }
  }



}
